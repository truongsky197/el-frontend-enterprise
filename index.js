$(document).ready(function(){
  var header = $(".list-qs");
  var fixedHead = $(".fixed-top");
	var widthScreen = $(this).width();
   var heightScreen = $(this).height();
	$('.login').css('min-height', heightScreen);

	add_star ();

	$('.ani').each(function() {
    $(this).click(function() {
      $(this).toggleClass('ani-active');
    })
  });

  var mtop = $('.link-for-course');
  if(widthScreen >= 992) {
    $('.learning-process').css('margin-top',mtop.height());
  }

  header.css('margin-top',fixedHead.height());

});

function add_star () {
  var starParent = $('.add-star');
  var i = 0;
  for(i = 0; i < 5; i++) {
    starParent.prepend("<i class='fas fa-star'></i>");
  }
}
